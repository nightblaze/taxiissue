It is necessary to create the application that implements authentication on the server.

The application must contain two screens:

* Screen for entering the e-mail and password (hereinafter - the login screen). It must contain an input field for e-mail (with a validation of the entered address), an input field for password (password field should masks the typed characters), a field for display information about errors and a button «Login»;

* Screen for display information about the successful authentication (hereinafter - the application screen) must contain an output field.

When you run the application, you need to check whether the authentication operation is performed before and expiration date of the access token (see api_token_expiration_date field in the response from the server authentication success (see. Description Protocol)).

If the entry was made previously and validity api token has not expired, you need to display the application screen with the date of expiry of the api_token_expiration_date. If the authentication operation has not been performed before, or the access token has expired, you need to display the login screen.
On the login screen, the user must enter the email address and password and after that he should to click on the button «Login» to perform authentication operation. If successful it's necessary to show the application screen with information about the successful entry and expiration date. In case of an error in the operation of authentication you should to display information about the error (error_description of body response from the server) in the output data input screen.

Protocol  Description

The interaction with the server takes WebSocket protocol (http://ru.wikipedia.org/wiki/WebSocket).

Request to the server and the responses from him are transferring in the JSON format (http://ru.wikipedia.org/wiki/JSON).

WebSocket URL: ws://xxx.xxx.xxx.xxxx:8080/customer-gateway/customer

Description of the structure of request:

```
{ 
  “type”: “TYPE_OF_MESSAGE” , // string, type of the message
  “sequence_id”: “09caaa73-b2b1-187e-2b24-683550a49b23”, // string, uuid
  “data” : {} // object, contains data of the request
}
```



Description of the structure of response:

```
{ 
  “type”: “TYPE_OF_MESSAGE” , // string, type of the message
  “sequence_id”: “09caaa73-b2b1-187e-2b24-683550a49b23”, // string, uuid
  “data” : {} // object, contains data of the response
}
```


E-mail and password for successful authentication:

"email":"fpi@bk.ru”,

"password":”123123"


E-mail and password for error authentication:

"email":"123@gmail.com”,

"password":”newPassword"