//
//  TAIMainWireframeDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentWireframeDelegate.h"

@protocol TAIMainWireframeDelegate <TAIParentWireframeDelegate>

@end
