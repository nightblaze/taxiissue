//
//  TAIMainWireframe.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIMainWireframe.h"

// Classes
#import "TAIMainPresenter.h"
#import "TAIMainViewController.h"
#import "TAIMainInteractor.h"
#import "TAIMainDataManager.h"

@implementation TAIMainWireframe

- (instancetype)init {
    self = [super init];
    if (self) {
        TAIMainPresenter *presenter = [[TAIMainPresenter alloc] init];
        TAIMainViewController *viewController = [TAIMainViewController viewController];
        TAIMainInteractor *interactor = [[TAIMainInteractor alloc] init];
        TAIMainDataManager *dataManager = [[TAIMainDataManager alloc] init];
        
        presenter.view = viewController;
        presenter.interactor = interactor;
        presenter.wireframe = self;
        
        viewController.presenter = presenter;
        
        interactor.presenter = presenter;
        interactor.dataManager = dataManager;
        
        dataManager.interactor = interactor;
        
        self.presenter = presenter;
    }
    return self;
}

@end
