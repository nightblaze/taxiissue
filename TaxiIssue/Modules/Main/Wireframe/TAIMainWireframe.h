//
//  TAIMainWireframe.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentWireframe.h"

// Interfaces
#import "TAIMainWireframeDelegate.h"

@protocol TAIMainPresenterDelegate;

@interface TAIMainWireframe : TAIParentWireframe <TAIMainWireframeDelegate>

@property (nonatomic, strong, nonnull) id<TAIMainPresenterDelegate> presenter;

@end
