//
//  TAIMainViewControllerDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentViewControllerDelegate.h"

@protocol TAIMainViewControllerDelegate <TAIParentViewControllerDelegate>

- (void)updateWelcomeLabel:(nullable NSString *)welcome;
- (void)updateExpirationDateLabel:(nullable NSString *)expirationDate;

@end
