//
//  TAIMainViewController.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIMainViewController.h"

// Interfaces
#import "TAIMainPresenterDelegate.h"

@interface TAIMainViewController ()

@property (nonatomic, weak, nullable) IBOutlet UILabel *welcomeLabel;
@property (nonatomic, weak, nullable) IBOutlet UILabel *expirationDateLabel;

@end

@implementation TAIMainViewController

#pragma mark - TAIMainViewControllerDelegate

- (void)updateWelcomeLabel:(nullable NSString *)welcome {
    self.welcomeLabel.text = welcome;
}

- (void)updateExpirationDateLabel:(nullable NSString *)expirationDate {
    self.expirationDateLabel.text = expirationDate;
}

@end
