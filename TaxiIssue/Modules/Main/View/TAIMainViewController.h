//
//  TAIMainViewController.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAIParentViewController.h"

// Interfaces
#import "TAIMainViewControllerDelegate.h"

@protocol TAIMainPresenterDelegate;

@interface TAIMainViewController : TAIParentViewController <TAIMainViewControllerDelegate>

@property (nonatomic, weak, nullable) id<TAIMainPresenterDelegate> presenter;

@end
