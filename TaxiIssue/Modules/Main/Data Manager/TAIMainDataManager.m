//
//  TAIMainDataManager.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIMainDataManager.h"

// Classes
#import "TAIUserService.h"

@interface TAIMainDataManager ()

@property (nonatomic, strong, nonnull) TAIUserService *userService;

@end

@implementation TAIMainDataManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userService = [[TAIUserService alloc] init];
    }
    return self;
}

#pragma mark - TAIMainDataManagerDelegate

- (nullable NSDate *)expirationDate {
    TAIUserCredentials *userCredentials = [self.userService credentials];
    return userCredentials.expirationDate;
}

@end
