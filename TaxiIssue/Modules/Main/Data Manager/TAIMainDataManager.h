//
//  TAIMainDataManager.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentDataManager.h"

// Interfaces
#import "TAIMainDataManagerDelegate.h"

@protocol TAIMainInteractorDelegate;

@interface TAIMainDataManager : TAIParentDataManager <TAIMainDataManagerDelegate>

@property (nonatomic, weak, nullable) id<TAIMainInteractorDelegate> interactor;

@end
