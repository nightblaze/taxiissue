//
//  TAIMainPresenter.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIMainPresenter.h"

// Interfaces
#import "TAIMainInteractorDelegate.h"
#import "TAIMainViewControllerDelegate.h"

@interface TAIMainPresenter ()

@property (nonatomic, strong, nonnull) NSDateFormatter *dateFormatter;

@end

@implementation TAIMainPresenter

- (instancetype)init {
    self = [super init];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        self.dateFormatter.timeStyle = NSDateFormatterMediumStyle;
        self.dateFormatter.locale = [NSLocale currentLocale];
        self.dateFormatter.timeZone = [NSTimeZone localTimeZone];
    }
    return self;
}

#pragma mark - Private methods

- (void)priv_setupInitialData {
    NSDate *expirationDate = [self.interactor expirationDate];
    NSString *expirationDatePrefix = @"Expiration date:";
    NSString *expirationDateString = @"Unknown";
    if (expirationDate) {
        expirationDateString = [self.dateFormatter stringFromDate:expirationDate];
    }
    [self.view updateExpirationDateLabel:[NSString stringWithFormat:@"%@ %@", expirationDatePrefix, expirationDateString]];
    
    [self.view updateWelcomeLabel:@"You are logged in"];
}

#pragma mark - TAIMainPresenterDelegate

- (void)viewDidLoadEvent {
    [self priv_setupInitialData];
}

@end
