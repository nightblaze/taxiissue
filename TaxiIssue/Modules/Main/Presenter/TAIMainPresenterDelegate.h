//
//  TAIMainPresenterDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentPresenterDelegate.h"

@protocol TAIMainPresenterDelegate <TAIParentPresenterDelegate>

@end
