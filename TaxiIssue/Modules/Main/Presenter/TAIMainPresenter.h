//
//  TAIMainPresenter.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentPresenter.h"

// Interfaces
#import "TAIMainPresenterDelegate.h"

@protocol TAIMainViewControllerDelegate;
@protocol TAIMainInteractorDelegate;
@protocol TAIMainWireframeDelegate;

@interface TAIMainPresenter : TAIParentPresenter <TAIMainPresenterDelegate>

@property (nonatomic, strong, nonnull) id<TAIMainViewControllerDelegate> view;
@property (nonatomic, strong, nonnull) id<TAIMainInteractorDelegate> interactor;
@property (nonatomic, strong, nonnull) id<TAIMainWireframeDelegate> wireframe;

@end
