//
//  TAIMainInteractor.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentInteractor.h"

// Interfaces
#import "TAIMainInteractorDelegate.h"

@protocol TAIMainPresenterDelegate;
@protocol TAIMainDataManagerDelegate;

@interface TAIMainInteractor : TAIParentInteractor <TAIMainInteractorDelegate>

@property (nonatomic, weak, nullable) id<TAIMainPresenterDelegate> presenter;
@property (nonatomic, strong, nonnull) id<TAIMainDataManagerDelegate> dataManager;

@end
