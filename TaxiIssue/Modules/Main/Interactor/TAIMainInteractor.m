//
//  TAIMainInteractor.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIMainInteractor.h"

// Interfaces
#import "TAIMainDataManagerDelegate.h"

@implementation TAIMainInteractor

#pragma mark - TAIMainInteractorDelegate

- (nullable NSDate *)expirationDate {
    return [self.dataManager expirationDate];
}

@end
