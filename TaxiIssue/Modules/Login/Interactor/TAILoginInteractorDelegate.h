//
//  TAILoginInteractorDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentInteractorDelegate.h"

@protocol TAILoginInteractorDelegate <TAIParentInteractorDelegate>

- (void)setEmail:(nullable NSString *)email;
- (void)setPassword:(nullable NSString *)password;
- (BOOL)isCorrectEmail;
- (BOOL)isCorrectPassword;
- (BOOL)isCorrectCreditianals;
- (void)login;

@end
