//
//  TAILoginInteractor.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentInteractor.h"

// Interfaces
#import "TAILoginInteractorDelegate.h"

@protocol TAILoginPresenterDelegate;
@protocol TAILoginDataManagerDelegate;

@interface TAILoginInteractor : TAIParentInteractor <TAILoginInteractorDelegate>

@property (nonatomic, weak, nullable) id<TAILoginPresenterDelegate> presenter;
@property (nonatomic, strong, nonnull) id<TAILoginDataManagerDelegate> dataManager;

@end
