//
//  TAILoginInteractor.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginInteractor.h"

// Categories
#import "NSString+TAIValidation.h"
#import "NSError+TAIFactory.h"

// Interfaces
#import "TAILoginDataManagerDelegate.h"
#import "TAILoginPresenterDelegate.h"

// Models
#import "TAILoginNetworkMessage.h"
#import "TAIUserCredentials.h"

@interface TAILoginInteractor ()

@property (nonatomic, strong, nullable) NSString *email;
@property (nonatomic, strong, nullable) NSString *password;

@end

@implementation TAILoginInteractor

#pragma mark - TAILoginInteractorDelegate

- (void)setEmail:(nullable NSString *)email {
    _email = email;
}

- (void)setPassword:(nullable NSString *)password {
    _password = password;
}

- (BOOL)isCorrectEmail {
    return [self.email tai_isValidEmail];
}

- (BOOL)isCorrectPassword {
    return self.password.length >= 3;
}

- (BOOL)isCorrectCreditianals {
    return [self isCorrectEmail] && [self isCorrectPassword];
}

- (void)login {
    TAIWeakify
    void (^loginDidFinishBlock)(TAIUserCredentials * _Nullable, NSError * _Nullable) = ^(TAIUserCredentials * _Nullable userCredentials, NSError * _Nullable error) {
        TAIStrongify
        [self.presenter loginDidFinishEventHandle];
        if (userCredentials && ![userCredentials isExpire]) {
            [self.presenter showCredentials:userCredentials];
        } else {
            NSError *newError;
            if (error) {
                newError = error;
            } else {
                newError = [NSError tai_errorWithLocalizedDescription:@"User credentials are expired"];
            }
            [self.presenter showError:newError];
        }
    };
    
    [self.dataManager login:self.email password:self.password success:^(TAIUserCredentials * _Nonnull userCredentials) {
        loginDidFinishBlock(userCredentials, nil);
    } failure:^(NSError * _Nonnull error) {
        loginDidFinishBlock(nil, error);
    }];
}

@end
