//
//  TAILoginDataManager.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentDataManager.h"

// Interfaces
#import "TAILoginDataManagerDelegate.h"

@protocol TAILoginInteractorDelegate;

@interface TAILoginDataManager : TAIParentDataManager <TAILoginDataManagerDelegate>

@property (nonatomic, weak, nullable) id<TAILoginInteractorDelegate> interactor;

@end
