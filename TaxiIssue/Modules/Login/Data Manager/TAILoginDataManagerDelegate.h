//
//  TAILoginDataManagerDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentDataManagerDelegate.h"

@class TAIUserCredentials;
@class TAIParentNetworkResult;

typedef void(^TAILoginDataManagerLoginSuccessBlock)(TAIUserCredentials * _Nullable userCredentials);
typedef void(^TAILoginDataManagerLoginErrorBlock)(NSError * _Nullable error);

@protocol TAILoginDataManagerDelegate <TAIParentDataManagerDelegate>

- (void)login:(nonnull NSString *)email
     password:(nonnull NSString *)password
      success:(nullable TAILoginDataManagerLoginSuccessBlock)success
      failure:(nullable TAILoginDataManagerLoginErrorBlock)failure;

@end
