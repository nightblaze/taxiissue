//
//  TAILoginDataManager.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginDataManager.h"

// Classes
#import "TAIUserService.h"

// Models
#import "TAILoginNetworkMessage.h"
#import "TAILoginNetworkResult.h"

@interface TAILoginDataManager ()

@property (nonatomic, strong, nonnull) TAIUserService *userService;

@end

@implementation TAILoginDataManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userService = [[TAIUserService alloc] init];
    }
    return self;
}

#pragma mark - TAILoginDataManagerDelegate

- (void)login:(nonnull NSString *)email
     password:(nonnull NSString *)password
      success:(nullable TAILoginDataManagerLoginSuccessBlock)success
      failure:(nullable TAILoginDataManagerLoginErrorBlock)failure {
    [self.userService login:email password:password success:^(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable result) {
        if (success) {
            TAIUserCredentials *userCredentials = ((TAILoginNetworkResult *)message.result).userCredentials;
            success(userCredentials);
        }
    } failure:^(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable error) {
        if (failure) {
            failure([error error]);
        }
    }];
}

@end
