//
//  TAILoginPresenterDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentPresenterDelegate.h"

@class TAIUserCredentials;

@protocol TAILoginPresenterDelegate <TAIParentPresenterDelegate>

- (void)emailWillChange:(nonnull NSString *)email;
- (void)passwordWillChange:(nonnull NSString *)password;
- (void)loginEventHandle;
- (void)showCredentials:(nonnull TAIUserCredentials *)userCredentials;
- (void)showError:(nonnull NSError *)error;
- (void)loginDidFinishEventHandle;

@end
