//
//  TAILoginPresenter.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentPresenter.h"

// Interfaces
#import "TAILoginPresenterDelegate.h"

@protocol TAILoginViewControllerDelegate;
@protocol TAILoginInteractorDelegate;
@protocol TAILoginWireframeDelegate;

@interface TAILoginPresenter : TAIParentPresenter <TAILoginPresenterDelegate>

@property (nonatomic, strong, nonnull) id<TAILoginViewControllerDelegate> view;
@property (nonatomic, strong, nonnull) id<TAILoginInteractorDelegate> interactor;
@property (nonatomic, strong, nonnull) id<TAILoginWireframeDelegate> wireframe;

@end
