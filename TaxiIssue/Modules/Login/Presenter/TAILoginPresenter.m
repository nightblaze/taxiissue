//
//  TAILoginPresenter.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginPresenter.h"

// Interfaces
#import "TAILoginViewControllerDelegate.h"
#import "TAILoginInteractorDelegate.h"
#import "TAILoginWireframeDelegate.h"

@implementation TAILoginPresenter

#pragma mark - Private methods

- (void)priv_setupInitialData {
    [self.view updateEmailLabel:@"Email"];
    [self.view updateErrorText:@""];
    [self.view updateErrorVisible:NO animated:NO];
    [self.view updateLoginButtonEnabled:NO];
    [self.view updateLoginButtonTitle:@"Login"];
    [self.view updatePasswordLabel:@"Password"];
    [self.view updateActivitIndicatorVisible:NO];
    
    [self.interactor setEmail:@""];
    [self.interactor setPassword:@""];
}

#pragma mark - TAILoginPresenterDelegate

- (void)viewDidLoadEvent {
    [self priv_setupInitialData];
}

- (void)emailWillChange:(nonnull NSString *)email {
    [self.interactor setEmail:email];
    [self.view updateLoginButtonEnabled:[self.interactor isCorrectCreditianals]];
}

- (void)passwordWillChange:(nonnull NSString *)password {
    [self.interactor setPassword:password];
    [self.view updateLoginButtonEnabled:[self.interactor isCorrectCreditianals]];
}

- (void)loginEventHandle {
    [self.view updateLoginButtonEnabled:NO];
    [self.view updateActivitIndicatorVisible:YES];
    [self.view updateErrorVisible:NO animated:YES];
    [self.interactor login];
}

- (void)showCredentials:(nonnull TAIUserCredentials *)userCredentials {
    [self.wireframe transitionToMain];
}

- (void)showError:(nonnull NSError *)error {
    [self.view showErrorMessage:error.localizedDescription];
    [self.view updateErrorVisible:YES animated:YES];
}

- (void)loginDidFinishEventHandle {
    [self.view updateLoginButtonEnabled:[self.interactor isCorrectCreditianals]];
    [self.view updateActivitIndicatorVisible:NO];
}

@end
