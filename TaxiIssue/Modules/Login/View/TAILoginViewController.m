//
//  TAILoginViewController.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginViewController.h"

// Categories
#import "UIColor+TAIColors.h"

// Interfaces
#import "TAILoginPresenterDelegate.h"

@interface TAILoginViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIView *inputsView;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UILabel *passwordLabel;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UILabel *loginLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *inputsViewVerticalConstraint;
@property (nonatomic, weak) IBOutlet UITextView *errorTextView;

@end

@implementation TAILoginViewController

#pragma mark - Actions

- (IBAction)act_loginButtonDidPress:(id)sender {
    [self priv_login];
}

#pragma mark - Private methods

- (void)priv_login {
    [self.view endEditing:NO];
    [self.presenter loginEventHandle];
}

#pragma mark - TAILoginViewControllerDelegate

- (void)updateEmailLabel:(nullable NSString *)label {
    self.emailLabel.text = label;
}

- (void)updatePasswordLabel:(nullable NSString *)label {
    self.passwordLabel.text = label;
}

- (void)updateLoginButtonTitle:(nullable NSString *)title {
    self.loginLabel.text = title;
}

- (void)updateLoginButtonEnabled:(BOOL)enabled {
    self.loginButton.enabled = enabled;
    self.loginLabel.textColor = enabled ? RGB(11, 97, 255) : RGB(123, 124, 129);
}

- (void)updateErrorText:(nullable NSString *)text {
    self.errorTextView.text = text;
}

- (void)updateErrorVisible:(BOOL)visible animated:(BOOL)animated {
    float alpha = visible ? 1.0f : 0.0f;
    NSTimeInterval duration = animated ? 0.35f : 0.0f;
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.errorTextView.alpha = alpha;
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)updateActivitIndicatorVisible:(BOOL)visible {
    if (visible) {
        [self.activityIndicatorView startAnimating];
    } else {
        [self.activityIndicatorView stopAnimating];
    }
}

- (void)showErrorMessage:(nonnull NSString *)message {
    [self updateErrorText:message];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.emailTextField) {
        [self.presenter emailWillChange:newText];
    } else {
        [self.presenter passwordWillChange:newText];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [self priv_login];
    }
    
    return NO;
}

@end
