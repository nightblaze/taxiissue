//
//  TAILoginViewController.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAIParentViewController.h"

// Interfaces
#import "TAILoginViewControllerDelegate.h"

@protocol TAILoginPresenterDelegate;

@interface TAILoginViewController : TAIParentViewController <TAILoginViewControllerDelegate>

@property (nonatomic, weak, nullable) id<TAILoginPresenterDelegate> presenter;

@end
