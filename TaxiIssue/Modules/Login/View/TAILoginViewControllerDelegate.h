//
//  TAILoginViewControllerDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentViewControllerDelegate.h"

@protocol TAILoginViewControllerDelegate <TAIParentViewControllerDelegate>

- (void)updateEmailLabel:(nullable NSString *)label;
- (void)updatePasswordLabel:(nullable NSString *)label;
- (void)updateLoginButtonTitle:(nullable NSString *)title;
- (void)updateLoginButtonEnabled:(BOOL)enabled;
- (void)updateErrorText:(nullable NSString *)text;
- (void)updateErrorVisible:(BOOL)visible animated:(BOOL)animated;
- (void)updateActivitIndicatorVisible:(BOOL)visible;

@end
