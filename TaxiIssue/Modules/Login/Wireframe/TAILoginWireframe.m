//
//  TAILoginWireframe.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginWireframe.h"

// Classes
#import "TAILoginPresenter.h"
#import "TAILoginViewController.h"
#import "TAILoginInteractor.h"
#import "TAILoginDataManager.h"

// Wireframes
#import "TAIMainWireframe.h"

@implementation TAILoginWireframe

- (instancetype)init {
    self = [super init];
    if (self) {
        TAILoginPresenter *presenter = [[TAILoginPresenter alloc] init];
        TAILoginViewController *viewController = [TAILoginViewController viewController];
        TAILoginInteractor *interactor = [[TAILoginInteractor alloc] init];
        TAILoginDataManager *dataManager = [[TAILoginDataManager alloc] init];
        
        presenter.view = viewController;
        presenter.interactor = interactor;
        presenter.wireframe = self;
        
        viewController.presenter = presenter;
        
        interactor.presenter = presenter;
        interactor.dataManager = dataManager;
        
        dataManager.interactor = interactor;
        
        self.presenter = presenter;
    }
    return self;
}

#pragma mark - TAILoginWireframeDelegate

- (void)transitionToMain {
    [TAIMainWireframe installAsRootWireframe];
}

@end
