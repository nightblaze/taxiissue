//
//  TAILoginWireframe.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentWireframe.h"

// Interfaces
#import "TAILoginWireframeDelegate.h"

@protocol TAILoginPresenterDelegate;

@interface TAILoginWireframe : TAIParentWireframe <TAILoginWireframeDelegate>

@property (nonatomic, strong, nonnull) id<TAILoginPresenterDelegate> presenter;

@end
