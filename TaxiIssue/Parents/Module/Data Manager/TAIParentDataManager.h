//
//  TAIParentDataManager.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Interfaces
#import "TAIParentDataManagerDelegate.h"

/**
 *  Parent class for data managers. Data managers are responsible for retrieve and store data.
 */
@interface TAIParentDataManager : NSObject <TAIParentDataManagerDelegate>

@end
