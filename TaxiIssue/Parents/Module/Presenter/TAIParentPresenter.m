//
//  TAIParentPresenter.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentPresenter.h"

// Frameworks
#import <UIKit/UIKit.h>

// Interfaces
#import "TAIParentViewControllerDelegate.h"

@interface TAIParentPresenter ()

@property (nonatomic, strong, nonnull) id<TAIParentViewControllerDelegate> view;

@end

@implementation TAIParentPresenter

#pragma mark - TAIParentPresenterDelegate

- (nonnull UIViewController *)viewController {
    if (![self.view isKindOfClass:[UIViewController class]]) {
        return [[UIViewController alloc] init];
    }
    
    return (UIViewController *)self.view;
}

- (void)viewDidLoadEvent {
    
}

@end
