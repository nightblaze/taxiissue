//
//  TAIParentPresenter.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Interfaces
#import "TAIParentPresenterDelegate.h"

/**
 *  Parent class for presenters. Presenters are responsible for telling the view what to display and handling events.
 */
@interface TAIParentPresenter : NSObject <TAIParentPresenterDelegate>

@end
