//
//  TAIParentPresenterDelegate.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@protocol TAIParentPresenterDelegate <NSObject>

- (nonnull UIViewController *)viewController;
- (void)viewDidLoadEvent;

@end
