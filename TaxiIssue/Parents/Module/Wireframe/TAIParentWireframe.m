//
//  TAIParentWireframe.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentWireframe.h"

// Classes
#import "TAIAppDelegate.h"

// Frameworks
#import <UIKit/UIKit.h>

// Interfaces
#import "TAIParentPresenterDelegate.h"

// View controllers
#import "TAIParentNavigationController.h"

@interface TAIParentWireframe ()

@property (nonatomic, strong, nonnull) id<TAIParentPresenterDelegate> presenter;

@end

@implementation TAIParentWireframe

#pragma mark - Custom accessors

- (nonnull UIViewController *)viewController {
    return [self.presenter viewController];
}

#pragma mark - Public methods

+ (void)installAsRootWireframe {
    TAIParentWireframe *wireframe = [[self alloc] init];
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [wireframe installRootViewController:wireframe.viewController intoWindow:window];
}

- (void)installRootViewController:(nonnull UIViewController *)viewController intoWindow:(nonnull UIWindow *)window {
    TAIParentNavigationController *navigationController = [[TAIParentNavigationController alloc] initWithRootViewController:viewController];
    window.rootViewController = navigationController;
    TAIAppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.window = window;
    [window makeKeyAndVisible];
}

- (void)pushWireframe:(nonnull TAIParentWireframe *)wireframe animated:(BOOL)animated {
    [self.viewController.navigationController pushViewController:wireframe.viewController animated:animated];
}

- (void)popWireframeAnimated:(BOOL)animated {
    [self.viewController.navigationController popViewControllerAnimated:animated];
}

@end
