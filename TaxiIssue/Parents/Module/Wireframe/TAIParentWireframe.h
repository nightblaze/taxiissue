//
//  TAIParentWireframe.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Interfaces
#import "TAIParentWireframeDelegate.h"

@class UIViewController;
@class UIWindow;

/**
 *  Parent class for wireframes. Wireframes are responsible for initializing all the other classes and handling routing to other views in the app.
 */
@interface TAIParentWireframe : NSObject <TAIParentWireframeDelegate>

@property (nonatomic, strong, readonly, nonnull) UIViewController *viewController;

+ (void)installAsRootWireframe;
- (void)installRootViewController:(nonnull UIViewController *)viewController intoWindow:(nonnull UIWindow *)window;
- (void)pushWireframe:(nonnull TAIParentWireframe *)wireframe animated:(BOOL)animated;
- (void)popWireframeAnimated:(BOOL)animated;

@end
