//
//  TAIParentViewController.h
//  TaxiIssue
//
//  Created by Andrey Timonenkov on 19.11.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

// Interfaces
#import "TAIParentViewControllerDelegate.h"

/**
 *  Parent class for view controllers. View controllers are responsible for displaying information to the user and detecting user interaction.
 */
@interface TAIParentViewController : UIViewController <TAIParentViewControllerDelegate>

+ (nonnull instancetype)viewController;
+ (nonnull NSString *)storyboardName;
+ (nonnull NSString *)identifier;

@end
