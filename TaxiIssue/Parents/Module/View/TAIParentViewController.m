//
//  TAIParentViewController.m
//  TaxiIssue
//
//  Created by Andrey Timonenkov on 19.11.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentViewController.h"

// Interfaces
#import "TAIParentPresenterDelegate.h"

@interface TAIParentViewController ()

@property (nonatomic, weak, nullable) id<TAIParentPresenterDelegate> presenter;

@end

@implementation TAIParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self.presenter viewDidLoadEvent];
}

#pragma mark - Public methods

+ (nonnull instancetype)viewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self storyboardName] bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:[self identifier]];
}

+ (nonnull NSString *)storyboardName {
    return @"Main";
}

+ (nonnull NSString *)identifier {
    return NSStringFromClass([self class]);
}

#pragma mark - TAIParentViewControllerDelegate

- (void)showErrorMessage:(nonnull NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
