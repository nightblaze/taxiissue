//
//  TAIParentInteractor.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Interfaces
#import "TAIParentInteractorDelegate.h"

/**
 *  Parent class for interactors. Interactors are responsible for performing business logic.
 */
@interface TAIParentInteractor : NSObject <TAIParentInteractorDelegate>

@end
