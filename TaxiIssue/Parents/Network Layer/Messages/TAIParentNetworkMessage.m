//
//  TAIParentNetworkMessage.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentNetworkMessage.h"

@interface TAIParentNetworkMessage ()

@property (nonatomic, strong, nonnull) NSString *serialized;

@end

@implementation TAIParentNetworkMessage

- (instancetype)init {
    self = [super init];
    if (self) {
        self.operationType = TAIOperationTypeNetwork;
        self.type = @"UNKNOWN";
        self.sequenceId = [NSUUID UUID].UUIDString;
        self.data = @{};
    }
    
    return self;
}

- (void)main {
    [self priv_serialize];
}

#pragma mark - Private methods

- (void)priv_serialize {
    NSDictionary *dictionary = @{@"type": self.type,
                                 @"sequence_id": self.sequenceId,
                                 @"data": self.data};
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    if (!data) {
        DDLogWarn(@"Error serialize message %@", error);
        data = [NSData data];
    }
    
    self.serialized = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@end
