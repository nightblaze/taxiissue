//
//  TAIParentNetworkMessage.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Classes
#import "TAIOperation.h"
#import "TAINetworkBlocks.h"
#import "TAIParentNetworkResult.h"

@interface TAIParentNetworkMessage : TAIOperation

@property (nonatomic, strong, nonnull) NSString *type;
@property (nonatomic, strong, nonnull) NSString *sequenceId;
@property (nonatomic, strong, nonnull) NSDictionary *data;
@property (nonatomic, strong, readonly, nonnull) NSString *serialized;
@property (nonatomic, strong, nullable) TAIParentNetworkResult *result;
@property (nonatomic, copy, nullable) TAINetworkManagerSuccessBlock successBlock;
@property (nonatomic, copy, nullable) TAINetworkManagerErrorBlock errorBlock;

@end
