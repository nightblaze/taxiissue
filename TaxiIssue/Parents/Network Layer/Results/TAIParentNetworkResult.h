//
//  TAIParentNetworkResult.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIOperation.h"

// Classes
#import "TAINetworkClassMapper.h"

@interface TAIParentNetworkResult : TAIOperation

@property (nonatomic, strong, readonly, nonnull) NSDictionary *deserialized;
@property (nonatomic, strong, readonly, nullable) NSString *type;
@property (nonatomic, strong, readonly, nullable) NSString *sequenceId;
@property (nonatomic, strong, readonly, nullable) NSDictionary *data;
@property (nonatomic, strong, readonly, nullable) NSString *errorCode;
@property (nonatomic, strong, readonly, nullable) NSString *errorDescription;
@property (nonatomic, strong, readonly, nullable) NSString *errorUUID;
@property (nonatomic, strong, readonly, nullable) NSArray *errors;
@property (nonatomic, strong, readonly, nullable) NSDictionary *event;

- (nonnull instancetype)init NS_UNAVAILABLE;
- (nonnull instancetype)initWithDeserialized:(nonnull NSDictionary *)deserialized NS_DESIGNATED_INITIALIZER;
- (BOOL)isOk;
- (nonnull NSError *)error;

@end
