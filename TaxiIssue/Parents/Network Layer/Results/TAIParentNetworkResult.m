//
//  TAIParentNetworkResult.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentNetworkResult.h"

// Categories
#import "NSError+TAIFactory.h"

@interface TAIParentNetworkResult ()

@property (nonatomic, strong, nonnull) NSDictionary *deserialized;
@property (nonatomic, strong, nullable) NSString *type;
@property (nonatomic, strong, nullable) NSString *sequenceId;
@property (nonatomic, strong, nullable) NSDictionary *data;
@property (nonatomic, strong, nullable) NSString *errorCode;
@property (nonatomic, strong, nullable) NSString *errorDescription;
@property (nonatomic, strong, nullable) NSString *errorUUID;
@property (nonatomic, strong, nullable) NSArray *errors;
@property (nonatomic, strong, nullable) NSDictionary *event;

@end

@implementation TAIParentNetworkResult

- (nonnull instancetype)initWithDeserialized:(nonnull NSDictionary *)deserialized {
    self = [super init];
    if (self) {
        self.operationType = TAIOperationTypeNetwork;
        self.deserialized = deserialized;
    }
    
    return self;
}

- (void)main {
    [self priv_deserialize];
}

#pragma mark - Public methods

- (BOOL)isOk {
    return self.errorCode.length == 0 && self.errorDescription.length == 0;
}

- (nonnull NSError *)error {
    NSError *result = nil;
    if (self.errorDescription) {
        result = [NSError tai_errorWithLocalizedDescription:self.errorDescription];
    } else {
        result = [NSError tai_errorWithLocalizedDescription:@"Unknown error"];
    }
    
    return result;
}

#pragma mark - Private methods

- (void)priv_deserialize {
    self.type = self.deserialized[@"type"];
    self.sequenceId = self.deserialized[@"sequence_id"];
    self.data = self.deserialized[@"data"];
    self.errorCode = self.data[@"error_code"];
    self.errorDescription = self.data[@"error_description"];
    self.errorUUID = self.data[@"error_uuid"];
    self.errors = self.data[@"errors"];
    self.event = self.data[@"in_event"];
}

@end
