//
//  TAIParentNavigationController.h
//  TaxiIssue
//
//  Created by Andrey Timonenkov on 19.11.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAIParentNavigationController : UINavigationController

@end
