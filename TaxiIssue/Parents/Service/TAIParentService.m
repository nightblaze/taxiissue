//
//  TAIParentService.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIParentService.h"

@interface TAIParentService ()

@property (nonatomic, strong, nonnull) TAINetworkManager *networkManager;

@end

@implementation TAIParentService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.networkManager = [TAINetworkManager sharedManager];
    }
    return self;
}

@end
