//
//  TAIParentService.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 09.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Classes
#import "TAINetworkManager.h"
#import "TAINetworkBlocks.h"

/**
 *  Parent class for services. Services execute network requests to the server for specific entities and perform fetches to the persistent store.
 */
@interface TAIParentService : NSObject

@property (nonatomic, strong, readonly, nonnull) TAINetworkManager *networkManager;

@end
