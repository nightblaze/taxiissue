//
//  NSError+TAIFactory.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (TAIFactory)

+ (nonnull NSError *)tai_errorWithLocalizedDescription:(nonnull NSString *)loalizedDescription;

@end
