//
//  NSError+TAIFactory.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "NSError+TAIFactory.h"

static NSString * const kTAINSErrorDomainName = @"com.ati-soft.TaxiIssue";

@implementation NSError (TAIFactory)

+ (nonnull NSError *)tai_errorWithLocalizedDescription:(nonnull NSString *)loalizedDescription {
    NSError *result = [NSError errorWithDomain:kTAINSErrorDomainName code:0 userInfo:@{NSLocalizedDescriptionKey: loalizedDescription}];
    
    return result;
}

@end
