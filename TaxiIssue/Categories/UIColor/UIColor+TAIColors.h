//
//  UIColor+TAIColors.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef RGB
#define RGB(r, g, b) [UIColor tai_colorWithRed:(r) green:(g) blue:(b)]
#endif

@interface UIColor (TAIColors)

+ (nonnull UIColor *)tai_colorWithRed:(unsigned char)red green:(unsigned char)green blue:(unsigned char)blue;

@end
