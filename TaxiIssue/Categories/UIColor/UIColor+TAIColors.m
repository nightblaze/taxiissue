//
//  UIColor+TAIColors.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "UIColor+TAIColors.h"

@implementation UIColor (TAIColors)

+ (nonnull UIColor *)tai_colorWithRed:(unsigned char)red green:(unsigned char)green blue:(unsigned char)blue {
    return [UIColor colorWithRed:(float)red / 255.0f green:(float)green / 255.0f blue:(float)blue / 255.0f alpha:1.0f];
}

@end
