//
//  NSString+TAIValidation.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TAIValidation)

- (BOOL)tai_isValidEmail;

@end
