//
//  TAIWeakStrongMacros.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#ifndef TaxiIssue_TAIWeakStrongMacros_h
#define TaxiIssue_TAIWeakStrongMacros_h


#define TAIWeakify __weak __typeof(self) wself = self;

#define TAIStrongify \
    _Pragma("clang diagnostic push") \
    _Pragma("clang diagnostic ignored \"-Wshadow\"") \
    __strong __typeof(wself) self = wself; \
    _Pragma("clang diagnostic pop")

#endif
