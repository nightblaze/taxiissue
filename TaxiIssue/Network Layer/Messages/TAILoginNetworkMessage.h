//
//  TAILoginNetworkMessage.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentNetworkMessage.h"

@interface TAILoginNetworkMessage : TAIParentNetworkMessage

@property (nonatomic, strong, nonnull) NSString *email;
@property (nonatomic, strong, nonnull) NSString *password;

@end
