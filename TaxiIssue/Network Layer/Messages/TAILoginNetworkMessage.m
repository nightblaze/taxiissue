//
//  TAILoginNetworkMessage.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginNetworkMessage.h"

@implementation TAILoginNetworkMessage

- (void)main {
    DDAssert(self.email, @"email should be not nil");
    DDAssert(self.password, @"password should be not nil");
    
    self.type = @"LOGIN_CUSTOMER";
    self.data = @{@"email": self.email,
                  @"password": self.password};
    
    [super main];
}

@end
