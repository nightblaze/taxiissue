//
//  TAINetworkClassMapper.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAINetworkClassMapper : NSObject

+ (void)addClass:(nonnull Class)class key:(nonnull NSString *)key;
+ (nullable Class)classForKey:(nonnull NSString *)key;

@end
