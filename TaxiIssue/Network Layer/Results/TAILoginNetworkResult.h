//
//  TAILoginNetworkResult.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentNetworkResult.h"

// Models
#import "TAIUserCredentials.h"

@interface TAILoginNetworkResult : TAIParentNetworkResult

@property (nonatomic, strong, nullable) NSString *accessToken;
@property (nonatomic, strong, nullable) NSDate *expirationDate;
@property (nonatomic, strong, nullable) TAIUserCredentials *userCredentials;

@end
