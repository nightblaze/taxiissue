//
//  TAILoginNetworkResult.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILoginNetworkResult.h"

@implementation TAILoginNetworkResult

+ (void)load {
    [TAINetworkClassMapper addClass:self key:@"CUSTOMER_API_TOKEN"];
}

- (void)main {
    [super main];
    
    self.accessToken = self.data[@"api_token"];
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    }
    NSString *dateString = self.data[@"api_token_expiration_date"];
    if (dateString) {
        self.expirationDate = [dateFormatter dateFromString:dateString];
    }
    TAIUserCredentials *userCredentials = [[TAIUserCredentials alloc] init];
    userCredentials.accessToken = self.accessToken;
    userCredentials.expirationDate = self.expirationDate;
    self.userCredentials = userCredentials;
}

@end
