//
//  TAINetworkBlocks.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#ifndef TAINetworkBlocks_h
#define TAINetworkBlocks_h

@class TAIParentNetworkMessage;
@class TAIParentNetworkResult;

typedef void(^TAINetworkManagerSuccessBlock)(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable result);
typedef void(^TAINetworkManagerErrorBlock)(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable error);

#endif /* TAINetworkBlocks_h */
