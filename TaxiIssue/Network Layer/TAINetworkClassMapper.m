//
//  TAINetworkClassMapper.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 15.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAINetworkClassMapper.h"

static NSMutableDictionary *classMapper;

@implementation TAINetworkClassMapper

+ (void)initialize {
    if (self == [TAINetworkClassMapper self]) {
        classMapper = [NSMutableDictionary dictionary];
    }
}

#pragma mark - Public methods

+ (void)addClass:(nonnull Class)class key:(nonnull NSString *)key {
    [classMapper setObject:class forKey:key];
}

+ (nullable Class)classForKey:(nonnull NSString *)key {
    id result = classMapper[key];
    if (result && result != [NSNull null]) {
        return result;
    }
    
    return nil;
}

@end
