//
//  TAINetworkManager.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAINetworkManager.h"

// Classes
#import <SocketRocket/SRWebSocket.h>

// Enums
#import "TAIOperationType.h"

// Managers
#import "TAIOperationManager.h"

// Models
#import "TAIParentNetworkMessage.h"
#import "TAIParentNetworkResult.h"
#import "TAIUnknownNetworkResult.h"

static NSString * const kTAINetworkServerAddress = @"ws://95.213.131.42:8080/customer-gateway/customer";

// SRWebSocket has strange socket state so better to use local state
typedef NS_ENUM(NSInteger, TAINetworkOperationSocketState) {
    TAINetworkOperationSocketStateUndefined,
    TAINetworkOperationSocketStateOpening,
    TAINetworkOperationSocketStateOpen,
    TAINetworkOperationSocketStateClosing,
    TAINetworkOperationSocketStateClosed
};

@interface TAINetworkManager () <SRWebSocketDelegate>

@property (strong, nonatomic, nonnull) TAIOperationManager *queueManager;
@property (strong, nonatomic, nonnull) SRWebSocket *socket;
@property (strong, nonatomic, nonnull) NSMutableDictionary *sendedMessages;
@property (atomic) TAINetworkOperationSocketState socketState;

@end

@implementation TAINetworkManager

+ (nonnull instancetype)sharedManager {
    static TAINetworkManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self priv_initialize];
    }
    return self;
}

#pragma mark - Public methods

- (void)openSocket {
    if (!self.socket) {
        return;
    }
    
    self.socketState = TAINetworkOperationSocketStateOpening;
    [self.socket open];
}

- (void)closeSocket {
    if (!self.socket) {
        return;
    }
    
    self.socketState = TAINetworkOperationSocketStateClosing;
    [self.socket close];
}

- (void)cancelAllRequests {
    [self.queueManager cancelOperationsOfType:TAIOperationTypeNetwork];
}

- (void)sendMessage:(nonnull TAIParentNetworkMessage *)message success:(nullable TAINetworkManagerSuccessBlock)success failure:(nullable TAINetworkManagerErrorBlock)failure {
    message.successBlock = success;
    message.errorBlock = failure;
    [self.queueManager addOperation:message finishedTarget:self action:@selector(priv_sendMessage:)];
}

#pragma mark - Private methods

- (void)priv_initialize {
    self.queueManager = [TAIOperationManager sharedManager];
    [self cancelAllRequests];
    
    [self priv_reinitSocket];
    
    self.sendedMessages = [NSMutableDictionary dictionary];
}

- (void)priv_reinitSocket {
    [self closeSocket];
    self.socketState = TAINetworkOperationSocketStateUndefined;
    self.socket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:kTAINetworkServerAddress]];
    self.socket.delegate = self;
}

- (void)priv_sendMessage:(nonnull TAIParentNetworkMessage *)message {
    DDAssert(message, @"message should not be nil");
    DDAssert(message.type.length, @"message tpe should not be empty");
    
    BOOL shouldRetrySendMessage = NO;
    if (self.socketState == TAINetworkOperationSocketStateClosed) {
        [self priv_reinitSocket];
    }
    if (self.socketState == TAINetworkOperationSocketStateUndefined || self.socketState == TAINetworkOperationSocketStateClosed) {
        [self openSocket];
        shouldRetrySendMessage = YES;
    } else if (self.socketState == TAINetworkOperationSocketStateOpening || self.socketState == TAINetworkOperationSocketStateClosing) {
        shouldRetrySendMessage = YES;
    }
    if (shouldRetrySendMessage) {
        [self performSelector:@selector(priv_sendMessage:) withObject:message afterDelay:1.0f];
        return;
    }
    self.sendedMessages[message.sequenceId] = message;
    [self.socket send:message.serialized];
}

- (void)priv_deserializingMessageDidFinish:(nonnull TAIParentNetworkResult *)result {
    TAIParentNetworkMessage *message = nil;
    if (result.sequenceId) {
        message = self.sendedMessages[result.sequenceId];
        [self.sendedMessages removeObjectForKey:result.sequenceId];
    }
    if (message) {
        message.result = result;
        if (message.successBlock && [result isOk]) {
            message.successBlock(message, result);
        } else if (message.errorBlock && ![result isOk]) {
            message.errorBlock(message, result);
        }
    } else {
        // TODO: If server pushes message to the client then the message will be nil. We should to handle it. But this is test issue and it is not covered in it.
    }
}

#pragma mark - SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    if (!message) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        id dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!dictionary) {
                DDLogWarn(@"Error deserialising message %@", error);
            } else if (![dictionary isKindOfClass:[NSDictionary class]]) {
                DDLogWarn(@"Received unsupported response %@", message);
            } else {
                id key = dictionary[@"type"];
                if (key && key != [NSNull null] && [key length] > 0) {
                    Class resultClass = [TAINetworkClassMapper classForKey:key];
                    if (!resultClass) {
                        resultClass = [TAIUnknownNetworkResult class];
                    }
                    if ([resultClass isSubclassOfClass:[TAIParentNetworkResult class]]) {
                        TAIParentNetworkResult *result = [[resultClass alloc] initWithDeserialized:dictionary];
                        [[TAIOperationManager sharedManager] addOperation:result finishedTarget:self action:@selector(priv_deserializingMessageDidFinish:)];
                    }
                }
            }
        });
    });
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    self.socketState = TAINetworkOperationSocketStateOpen;
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    self.socketState = TAINetworkOperationSocketStateClosed;
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    self.socketState = TAINetworkOperationSocketStateClosed;
}

@end
