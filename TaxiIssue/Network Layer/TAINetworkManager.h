//
//  TAINetworkManager.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAINetworkBlocks.h"

@class TAIParentNetworkMessage;

@interface TAINetworkManager : NSObject

+ (nonnull instancetype)sharedManager;
- (void)openSocket;
- (void)closeSocket;
- (void)cancelAllRequests;
- (void)sendMessage:(nonnull TAIParentNetworkMessage *)message success:(nullable TAINetworkManagerSuccessBlock)success failure:(nullable TAINetworkManagerErrorBlock)failure;

@end
