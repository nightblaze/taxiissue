//
//  TAIStartWindowConfigurator.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Interfaces
#import "TAIConfiguratorDelegate.h"

@interface TAIStartWindowConfigurator : NSObject <TAIConfiguratorDelegate>

@end
