//
//  TAIStartWindowConfigurator.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIStartWindowConfigurator.h"

// Classes
#import "TAIUserService.h"

// Frameworks
#import <UIKit/UIKit.h>

// Wireframes
#import "TAILoginWireframe.h"
#import "TAIMainWireframe.h"

@implementation TAIStartWindowConfigurator

#pragma mark - Private methods

+ (void)priv_configureStartWindow {
    TAIUserService *userService = [[TAIUserService alloc] init];
    if ([userService isLoggedIn]) {
        [TAIMainWireframe installAsRootWireframe];
    } else {
        [TAILoginWireframe installAsRootWireframe];
    }
}

#pragma mark - TAIConfiguratorDelegate

+ (void)configure {
    [self priv_configureStartWindow];
}

@end
