//
//  TAIUserService.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAIParentService.h"

// Models
#import "TAIUserCredentials.h"

@interface TAIUserService : TAIParentService

- (void)login:(nonnull NSString *)email
     password:(nonnull NSString *)password
      success:(nullable TAINetworkManagerSuccessBlock)success
      failure:(nullable TAINetworkManagerErrorBlock)failure;
- (BOOL)isLoggedIn;

// Let user service will be like 'store manager' for user credentials. It only because of test issue. In real project we need to create store manager
- (nullable TAIUserCredentials *)credentials;
- (void)updateCredentials:(nullable TAIUserCredentials *)credentials;

@end
