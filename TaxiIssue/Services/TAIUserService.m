//
//  TAIUserService.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIUserService.h"

// Messages
#import "TAILoginNetworkMessage.h"
#import "TAILoginNetworkResult.h"

static NSString * const kTAIUserServiceUserCredentialsKey = @"kTAIUserServiceUserCredentialsKey";

@interface TAIUserService ()

@property (nonatomic, strong, nonnull) NSUserDefaults *userDefaults;

@end

@implementation TAIUserService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

#pragma mark - Public methods

- (void)login:(nonnull NSString *)email
     password:(nonnull NSString *)password
      success:(nullable TAINetworkManagerSuccessBlock)success
      failure:(nullable TAINetworkManagerErrorBlock)failure {
    TAILoginNetworkMessage *networkMessage = [[TAILoginNetworkMessage alloc] init];
    networkMessage.email = email;
    networkMessage.password = password;
    
    [self.networkManager sendMessage:networkMessage success:^(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable result) {
        TAIUserCredentials *userCredentials = ((TAILoginNetworkResult *)message.result).userCredentials;
        [self updateCredentials:userCredentials];
        if (success) {
            success(message, result);
        }
    } failure:^(TAIParentNetworkMessage * _Nullable message, TAIParentNetworkResult * _Nullable error) {
        if (failure) {
            failure(message, error);
        }
    }];
}

- (BOOL)isLoggedIn {
    TAIUserCredentials *userCredentials = [self credentials];
    return userCredentials.accessToken.length > 0 && ![userCredentials isExpire];
}

- (nullable TAIUserCredentials *)credentials {
    NSData *encodedObject = [self.userDefaults objectForKey:kTAIUserServiceUserCredentialsKey];
    TAIUserCredentials *result = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return result;
}

- (void)updateCredentials:(nullable TAIUserCredentials *)credentials {
    NSData *encodedObject = nil;
    if (credentials) {
        encodedObject = [NSKeyedArchiver archivedDataWithRootObject:credentials];
    }
    [self.userDefaults setObject:encodedObject forKey:kTAIUserServiceUserCredentialsKey];
    [self.userDefaults synchronize];
}

@end
