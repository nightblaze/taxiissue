//
//  main.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAIAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAIAppDelegate class]));
    }
}
