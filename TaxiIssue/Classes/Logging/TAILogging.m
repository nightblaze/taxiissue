//
//  TAILogging.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAILogging.h"

// Classes
#import "CRLMethodLogFormatter.h"

@implementation TAILogging

#pragma mark - Public methods

+ (void)setupLogging {
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        CRLMethodLogFormatter *formatter = [[CRLMethodLogFormatter alloc] init];
        
        DDASLLogger *aslLogger = [DDASLLogger sharedInstance];
        aslLogger.logFormatter = formatter;
        [DDLog addLogger:aslLogger withLevel:DDLogLevelVerbose];
        
        DDTTYLogger *ttyLogger = [DDTTYLogger sharedInstance];
        ttyLogger.logFormatter = formatter;
        [ttyLogger setColorsEnabled:YES];
        [DDLog addLogger:ttyLogger withLevel:DDLogLevelVerbose];
        
        DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
        fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
        [DDLog addLogger:fileLogger withLevel:DDLogLevelInfo];
    });
}

@end
