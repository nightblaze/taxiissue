//
//  TAIOperationType.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#ifndef TAIOperationType_h
#define TAIOperationType_h

typedef NS_ENUM(NSInteger, TAIOperationType) {
    TAIOperationTypeUndefined,
    TAIOperationTypeNetwork,
    TAIOperationTypeCache
};

#endif /* TAIOperationType_h */
