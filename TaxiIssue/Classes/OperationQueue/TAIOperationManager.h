//
//  TAIOperationManager.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Enums
#import "TAIOperationType.h"

@class TAIOperation;

@interface TAIOperationManager : NSObject

+ (nonnull instancetype)sharedManager;
- (void)addOperation:(nonnull TAIOperation *)operation finishedTarget:(nonnull id)target action:(nonnull SEL)action;
- (void)cancelAllOperations;
- (void)cancelOperationsOfType:(TAIOperationType)type;

@end
