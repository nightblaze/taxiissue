//
//  TAIOperation.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIOperation.h"

@implementation TAIOperation

- (instancetype)init {
    self = [super init];
    if (self) {
        self.uuid = [NSUUID UUID].UUIDString;
    }
    return self;
}

- (void)main {
    DDAssert(NO, @"This is an abstract class, processing allowed only in subclasses.");
}

@end
