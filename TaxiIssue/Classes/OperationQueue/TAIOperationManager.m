//
//  TAIOperationManager.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIOperationManager.h"

// Models
#import "TAIOperation.h"

static NSString * const kTAIOperationOperationKVOKeyPath = @"isFinished";

@interface TAIOperationManager ()

@property (nonatomic, strong, nonnull) NSOperationQueue *queue;
@property (nonatomic, strong, nonnull) NSOperationQueue *heavyQueue;
@property (nonatomic, strong, nonnull) NSMutableDictionary *runningOperationToTargetMap;
@property (nonatomic, strong, nonnull) NSMutableDictionary *runningOperationToActionMap;
@property (nonatomic, strong, nonnull) NSMutableDictionary *runningOperationToIDMap;

@end

@implementation TAIOperationManager

+ (nonnull instancetype)sharedManager {
    static TAIOperationManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.queue = [[NSOperationQueue alloc] init];
        [self.queue setMaxConcurrentOperationCount:1];
        
        self.heavyQueue = [[NSOperationQueue alloc] init];
        [self.heavyQueue setMaxConcurrentOperationCount:1];
        
        self.runningOperationToActionMap = [NSMutableDictionary new];
        self.runningOperationToTargetMap = [NSMutableDictionary new];
        self.runningOperationToIDMap = [NSMutableDictionary new];
    }
    return self;
}

#pragma mark - Public methods

- (void)addOperation:(nonnull TAIOperation *)operation finishedTarget:(nonnull id)target action:(nonnull SEL)action {
    NSOperationQueue *queue = operation.isHeavy ? self.heavyQueue : self.queue;
    [self priv_addOperation:operation toQueue:queue finishedTarget:target action:action];
}

- (void)cancelAllOperations {
    [self.queue cancelAllOperations];
    [self.heavyQueue cancelAllOperations];
}

- (void)cancelOperationsOfType:(TAIOperationType)type {
    NSMutableArray *operations = [[NSMutableArray alloc] init];
    for (NSString *key in self.runningOperationToIDMap) {
        TAIOperation *operation = self.runningOperationToIDMap[key];
        if (operation.operationType == type) {
            [operations addObject:operation];
        }
    }
    for (TAIOperation *operation in operations) {
        [operation cancel];
    }
}

#pragma mark - Private methods

- (void)priv_addOperation:(nonnull TAIOperation *)operation
                  toQueue:(nonnull NSOperationQueue *)queue
           finishedTarget:(nonnull id)target
                   action:(nonnull SEL)action {
    DDAssert(operation, @"operation should not be nil");
    DDAssert(operation.uuid, @"operation uuid should not be nil");
    DDAssert(queue, @"queue should not be nil");
    DDAssert(target, @"target should not be nil");
    DDAssert(action, @"action should not be nil");
    
    self.runningOperationToIDMap[operation.uuid] = operation;
    self.runningOperationToActionMap[operation.uuid] = [NSValue valueWithPointer:action];
    self.runningOperationToTargetMap[operation.uuid] = target;
    
    [operation addObserver:self forKeyPath:kTAIOperationOperationKVOKeyPath options:NSKeyValueObservingOptionNew context:(__bridge void *)(queue)];
    
    [queue addOperation:operation];
}

- (void)priv_operationDone:(nonnull TAIOperation *)operation {
    id target;
    SEL action;
    
    target = self.runningOperationToTargetMap[operation.uuid];
    action = [self.runningOperationToActionMap[operation.uuid] pointerValue];
    
    [self.runningOperationToActionMap removeObjectForKey:operation.uuid];
    [self.runningOperationToTargetMap removeObjectForKey:operation.uuid];
    [self.runningOperationToIDMap removeObjectForKey:operation.uuid];
    
    if (![operation isCancelled]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:action onThread:[NSThread mainThread] withObject:operation waitUntilDone:NO];
#pragma clang diagnostic pop
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSString*, id> *)change context:(nullable void *)context {
    if ([keyPath isEqual:kTAIOperationOperationKVOKeyPath]) {
        TAIOperation *operation = (TAIOperation *)object;
        [operation removeObserver:self forKeyPath:kTAIOperationOperationKVOKeyPath];
        [self priv_operationDone:operation];
    }
}

@end
