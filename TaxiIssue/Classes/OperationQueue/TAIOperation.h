//
//  TAIOperation.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 13.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

// Enums
#import "TAIOperationType.h"

@interface TAIOperation : NSOperation

@property (nonatomic, strong, nonnull) NSString *uuid;
@property (nonatomic) TAIOperationType operationType;
@property (nonatomic) BOOL isHeavy;

@end
