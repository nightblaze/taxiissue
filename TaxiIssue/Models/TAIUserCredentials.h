//
//  TAIUserCredentials.h
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAIUserCredentials : NSObject <NSCoding>

@property (nonatomic, strong, nullable) NSString *accessToken;
@property (nonatomic, strong, nullable) NSDate *expirationDate;

- (BOOL)isExpire;

@end
