//
//  TAIUserCredentials.m
//  TaxiIssue
//
//  Created by Alexander Timonenkov on 14.12.15.
//  Copyright © 2015 ATi Soft. All rights reserved.
//

#import "TAIUserCredentials.h"

static NSString * const kTAIUserCredentialsAccessTokenKey = @"kTAIUserCredentialsAccessTokenKey";
static NSString * const kTAIUserCredentialsExpirationDateKey = @"kTAIUserCredentialsExpirationDateKey";

@implementation TAIUserCredentials

#pragma mark - Public methods

- (BOOL)isExpire {
    return [self.expirationDate timeIntervalSinceNow] < 0.0;
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.accessToken forKey:kTAIUserCredentialsAccessTokenKey];
    [aCoder encodeObject:self.expirationDate forKey:kTAIUserCredentialsExpirationDateKey];
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    TAIUserCredentials *userCredentials = [[TAIUserCredentials alloc] init];
    if (userCredentials) {
        userCredentials.accessToken = [aDecoder decodeObjectForKey:kTAIUserCredentialsAccessTokenKey];
        userCredentials.expirationDate = [aDecoder decodeObjectForKey:kTAIUserCredentialsExpirationDateKey];
    }
    
    return userCredentials;
}

@end
